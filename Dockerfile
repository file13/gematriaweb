FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/gematriaweb.jar /gematriaweb/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/gematriaweb/app.jar"]
