# gematriaweb

This is a Luminus web app of the [gematria program](https://gitlab.com/file13/gematria). It's basically an example
of a Clojure webapp using luminus and h2.

There's a live version over here:

[https://gematriaweb.herokuapp.com/](https://gematriaweb.herokuapp.com/)

## TODO

* I need to redo the database.
* I also would like to seperate the results by language.

## License

Copyright © 2017 Michael Rossi

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
