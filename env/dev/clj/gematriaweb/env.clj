(ns gematriaweb.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [gematriaweb.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[gematriaweb started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[gematriaweb has shut down successfully]=-"))
   :middleware wrap-dev})
