(ns user
  (:require [mount.core :as mount]
            [gematriaweb.db.core :refer :all]
            [gematria.core :as gem]
            gematriaweb.core))

(defn start []
  (mount/start-without #'gematriaweb.core/http-server
                       #'gematriaweb.core/repl-server))

(defn stop []
  (mount/stop-except #'gematriaweb.core/http-server
                     #'gematriaweb.core/repl-server))

(defn restart []
  (stop)
  (start))

;; (require '[gematriaweb.db.core :refer :all])
