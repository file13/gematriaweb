(ns gematriaweb.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[gematriaweb started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[gematriaweb has shut down successfully]=-"))
   :middleware identity})
