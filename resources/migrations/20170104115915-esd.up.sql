CREATE TABLE esd
       (Word VARCHAR(90),
        Number INTEGER,
        NumberList VARCHAR(250),
        Factors VARCHAR(250),
        DigitalRoot INTEGER,
        DigitSum INTEGER,
        NumberLength INTEGER,
        WordLength INTEGER);

RUNSCRIPT FROM 'resources/migrations/esd.sql';
