-- :name get-english-num :?
-- :doc get the english gematria for :number
SELECT word FROM esd
WHERE number = :number

-- :name get-greek-num :?
-- :doc get the greek gematria for :number
SELECT word FROM gsd
WHERE number = :number

-- :name get-hebrew-num :?
-- :doc get the hebrew gematria for :number
SELECT word FROM hsd
WHERE number = :number

-- :name get-all-num :?
-- :doc get's all the gematria words for :number
SELECT word FROM esd WHERE number = :number
UNION SELECT word from gsd WHERE number = :number
UNION SELECT word from hsd WHERE number = :number
ORDER BY word;
