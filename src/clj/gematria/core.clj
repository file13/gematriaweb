(ns gematria.core
  (:require
   ;; General
   ;;[clojure.pprint :refer :all]
   [clojure.string :as str]
   ))

(defn digital-root
  "Returns the digital root of an integer. 
  N must be an integer."
  [n]
  (if (<= n 0)
    0
    (let [r (mod n 9)]
      (if (zero? r)
        9
        r))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; English Gematria
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn english-gematria-basic
  "Returns the basic English gematria number for the character 'c'."
  [c]
  (let [magic-ascii 64   ; this is A - 1
        number-of-english-letters 26
        r (- (int (Character/toUpperCase c)) magic-ascii)]
    (if (and (>= r 1) (<= r number-of-english-letters))
      r
      0)))

(defn gematria
  "Returns the gematria sum of word based on the gematria function fun. 
  The gematria fun should take a character as input and return a number as 
  output. 
  If :as-list is true, it returns the list of gematria values for each 
  character. 
  If :as-both is true, returns a vector of the sum and the list."
  [fun word & {:keys [as-list as-both]
               :or   {as-list false as-both false}}]
  (cond
    as-both (let [glist (map fun (map char word))]
              [(reduce + glist) glist])
    as-list (map fun (map char word))
    :default (reduce + (map fun (map char word)))))

(defn english-char?
  "Predicate to see if a character is standard ASCII."
  [c]
  (let [code (int c)]
    (and (>= code 0) (<= code 127))))

(defn english-str?
  "Predicate to see if a string is standard ASCII."
  [str]
  (every? english-char? str))

(defn normalize-nfd
  "Normalizes a unicode string using NFD unicode formatting.
  http://www.unicode.org/reports/tr15/"
  [str]
  (java.text.Normalizer/normalize str java.text.Normalizer$Form/NFD))

(defn normalize-english-char
  "Removes any unicode accents or ligatures. Just returns the basic
  lower case character."
  [c]
  (let [lower (Character/toLowerCase c)]
    (if (english-char? lower)
      lower
      (first (normalize-nfd (str (Character/toLowerCase lower)))))))

(defn normalize-english
  "Normalizes an English string."
  [str]
  (str/join (map normalize-english-char (seq str))))

(defn english-basic-of-string
  "Computes the basic English gematria of a string."
  [str]
  (gematria english-gematria-basic (normalize-english str) :as-both true))

(defn english-gematria-standard
  "Returns the standard English gematria number for the character 'c'."
  [c]
  (let [n (english-gematria-basic c)]
    (cond (< n 1)  0
          (< n 11) n
          (< n 19) (* 10 (digital-root n))
          (< n 27) (* 100 (digital-root n))
          :default 0)))

(defn english-standard-of-string
  "Computes the standard English gematria of a string."
  [str]
  (gematria english-gematria-standard (normalize-english str) :as-both true))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; General unicode tools. Note used in the program,
;;; but useful while exploring unicode.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn codepoint-count
  "Returns the number of unicode codepoints of a string."
  [s]
  (.codePointCount s 0 (count s)))

(defn nth-codepoint
  "Gets the nth code point from a string."
  [s index]
  (.codePointAt s (.offsetByCodePoints s 0 index)))

(defn codepoint-seq
  "Returns a seq of unicode point ints."
  [s]
  (seq (.toArray (.codePoints s))))

(defn utf16-codepoint-seq
  "Returns a seq of utf-16 unicode point ints."
  [s]
  (map #(nth-codepoint (str %) 0) s))

(defn codepoint-hexes
  "Returns a list of the unicode code points of a string as hex strings."
  [s]    
  (map #(format "U+%04X" %) (codepoint-seq s)))

(defn utf16-codepoint-hexes
  "Returns a list of the unicode code points of a string as hex strings."
  [s]    
  (map #(format "U+%04X" %) (utf16-codepoint-seq s)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Greek Gematria
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn greek-char?
  "Predicate to see if a character is Unicode Greek or extended."
  [c]
  (let [code (int c)]
    (or (and (>= code 880) (<= code 1023))     ; greek
        (and (>= code 7936) (<= code 8191))))) ; extended

(defn greek-str?
  "Predicate to see if the entire string is Greek."
  [str]
  (every? greek-char? str))

(defn normalized-greek-char?
  "Predicate to check if the character is a lowercase 
  Greek letter with no markings."
  [c]
  (let [i (int c)]
    (or (and (>= i 945) (<= i 969))
        (= i 985) (= i 991)
        (= i 989))))

;;; NEED TESTS FROM HERE ON DOWN
(defn normalized-greek?
  "Predicate to see if a string is normalized Greek."
  [str]
  (every? normalized-greek-char? (seq str)))

(defn normalize-greek-char
  "Normalizes a Greek character."
  [c]
  (let [lower (Character/toLowerCase c)]
    (if (normalized-greek-char? lower)
      lower
      (first (normalize-nfd (str (Character/toLowerCase lower)))))))

(defn normalize-greek
  "Normalizes an Greek string."
  [str]
  (str/join (map normalize-greek-char (seq str))))

(defn greek-gematria-standard
  "Computes Greek gematria using the standard method 
  for a given character."
  [c]
  (let [i (int (normalize-greek-char c))]
    (case i
      945 1
      946 2
      947 3
      948 4
      949 5
      989 6
      950 7
      951 8
      952 9
      953 10
      954 20
      955 30
      956 40
      957 50
      958 60
      959 70
      960 80
      985 90
      991 90
      961 100
      962 200
      963 200
      964 300
      965 400
      966 500
      967 600
      968 700
      969 800
      993 900
      0)))

(defn greek-standard-of-string
  "Standard function to get the standard Greek gematria of a string."
  [str]
  (gematria greek-gematria-standard (normalize-greek str) :as-both true))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Hebrew Gematria
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn hebrew-char?
  "Predicate to see if a character is Unicode Hebrew."
  [c]
  (let [code (int c)]
    (or (and (>= code 1424) (<= code 1535))
        (and (>= code 64256) (<= code 64335)))))

(defn hebrew-str?
  "Predicate to see if the entire string is Greek."
  [str]
  (every? hebrew-char? str))

(defn normalized-hebrew-char?
  "Predicate to check if the character is a Hebrew letter 
  with no markings."
  [c]
  (let [i (int c)]
    (and (>= i 1487) (<= i 1515))))

(defn normalized-hebrew?
  "Predicate to see if a string is normalized Greek."
  [str]
  (every? normalized-hebrew-char? (seq str)))

(defn normalize-hebrew
  "Normalizes a Hebrew string."
  [s]
  (str/join (filter #(normalized-hebrew? (str %)) s)))
;;(normalize-hebrew "בְּרֵאשִׁית")

(defn hebrew-gematria-basic
  "Computes Hebrew gematria using the basic method 
  for a given character."
  [c]
  (let [i (int (normalize-greek-char c))]
    (case i
      1488   1
      1489   2
      1490   3
      1491   4
      1492   5
      1493   6
      1494   7
      1495   8
      1496   9
      1497  10
      1498  20
      1499  20
      1500  30
      1501  40
      1502  40
      1503  50
      1504  50
      1505  60
      1506  70
      1507  80
      1508  80
      1509  90
      1510  90
      1511 100
      1512 200
      1513 300
      1514 400
      0)))

(defn hebrew-gematria-standard
  "Computes Hebrew gematria using the Standard method 
  for a given character. By standard, we mean Mispar 
  gadol style, where final character values change."
  [c]
  (let [i (int (normalize-greek-char c))]
    (case i
      1488   1
      1489   2
      1490   3
      1491   4
      1492   5
      1493   6
      1494   7
      1495   8
      1496   9
      1497  10
      1499  20
      1500  30
      1502  40
      1504  50
      1505  60
      1506  70
      1508  80
      1510  90
      1511 100
      1512 200
      1513 300
      1514 400
      1498 500
      1501 600
      1503 700
      1507 800
      1509 900
      0)))

(defn hebrew-basic-of-string
  "Standard function to get the standard Hebrew gematria of a string."
  [str]
  (gematria hebrew-gematria-basic (normalize-hebrew str) :as-both true))

(defn hebrew-standard-of-string
  "Standard function to get the standard Hebrew gematria of a string."
  [str]
  (gematria hebrew-gematria-standard (normalize-hebrew str) :as-both true))

(defn parse-int
  "Helper to convert a string to an int or nil."
  [s]
  (if-let [n (re-find  #"\d+" s)]
    (Integer. n)))
