(ns gematriaweb.db.core
  (:require
    [conman.core :as conman]
    [mount.core :refer [defstate]]
    [gematriaweb.config :refer [env]]))

(defstate ^:dynamic *db*
           :start (conman/connect! {:jdbc-url (env :database-url)})
           :stop (conman/disconnect! *db*))

(conman/bind-connection *db* "sql/queries.sql")

;; (require '[gematriaweb.db.core :refer :all])
;; (mount/start #'gematriaweb.db.core/*db*)
