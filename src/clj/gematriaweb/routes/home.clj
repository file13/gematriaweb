(ns gematriaweb.routes.home
  (:require [gematriaweb.layout :as layout]
            [compojure.core :refer [defroutes GET]]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            ;; added
            [gematria.core :as gem]
            [gematriaweb.db.core :as db]
            [clojure.pprint :refer :all]
            [bouncer.core :as b]
            [bouncer.validators :as v]
            ))

;;; Currently the lookups do a get-all-num, which dosen't make
;;; for nice formatting.  I'll probably re-write to use
;;; a seperate SQL query for each language to seperate the results.

(defn sql-number-lookup
  [num]
  (db/get-all-num {:number num}))

(defn sql-lookup-str
  [fn s]
  (let [lookup (fn s)
        num (first lookup)
        num-list (cl-format false "~A" (second lookup))
        words (db/get-all-num {:number num})]
    {:word s :num num :num-list num-list :results words}
    ))

(defn sql-word-lookup [word]
  (cond
    (gem/greek-str? word)
    (sql-lookup-str gem/greek-standard-of-string word)

    (gem/hebrew-str? word)
    (sql-lookup-str gem/hebrew-standard-of-string word)

    :else (sql-lookup-str gem/english-standard-of-string word)))

(defn validate-lookup [params]
  (first
   (b/validate
    params
    :query [v/required [v/max-count 1024]])))

(defn home-page [{:keys [params]}]
  ;;(println (:query params))
  (if-let [errors (validate-lookup params)]
    ;; just ignore the input
    (-> (layout/render "home.html"
                       {:results nil}))
    (do
      (if-let [result (:query params)]
        (if-let [num (gem/parse-int result)]
          (layout/render "home.html"
                         {:num num
                          :results (sql-number-lookup num)}
                         )
          (layout/render "home.html" (sql-word-lookup result))
          )
        (layout/render "home.html"
                       {:results nil}
                       )))))

(defn about-page []
  (layout/render "about.html"))

(defroutes home-routes
  (GET "/" [] (home-page []))
  (GET "/about" [] (about-page))
  (GET "/lookup" request (home-page request))
  )
