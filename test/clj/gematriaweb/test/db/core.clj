(ns gematriaweb.test.db.core
  (:require [gematriaweb.db.core :refer [*db*] :as db]
            [luminus-migrations.core :as migrations]
            [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [gematriaweb.config :refer [env]]
            [mount.core :as mount]))

(use-fixtures
  :once
  (fn [f]
    (mount/start
      #'gematriaweb.config/env
      #'gematriaweb.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(deftest test-users
  (jdbc/with-db-transaction [t-conn *db*]
    (jdbc/db-set-rollback-only! t-conn)
    (is (= (map :word (db/get-english-num t-conn {:number 26}))
           '("back" "bagged" "caddie" "cicadae" "deice"
             "efface" "fief" "fife" "haddah" "hadid"
             "hagia" "hide" "hied" "jibe" "jig")))
    (is (= (map :word (db/get-greek-num t-conn {:number 26}))
           '("αγγιθ" "αιει" "ζαβαδια" "ιαδαι" "ιαδια")))
    (is (= (map :word (db/get-hebrew-num t-conn {:number 26}))
           '("חחי" "אחזי" "גוזי"
             "וגזי" "בידי" "החגי" "הביט" "יאחז" "והבוז" "ויגז"
             "בחיו" "והטו" "בוזהו" "וידו" "יודו" "בחטאו" "אכה"
             "הויה" "והיה" "חגיה" "וחזה" "חוזה" "יהוה" "והדוה"
             "והגבהה" "ויגבה" "וידאה" "בכד" "בדויד" "חדיד" "וההוד"
             "ויבאגד" "כבד" "היטב" "כדב" "ויובב" "ובחטא" "דיהוא" "דידהבא")))
    (is (= (map :word (db/get-all-num t-conn {:number 1}))
           '("a" "α" "א")))
    ))
