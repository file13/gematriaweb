(ns gematriaweb.test.handler
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [gematriaweb.handler :refer :all]))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 200 (:status response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response)))))

  (testing "number lookup"
    (let [response ((app) (request :get "/lookup?query=26"))]
      (is (= 200 (:status response)))))

  (testing "english lookup"
    (let [response ((app) (request :get "/lookup?query=Archie"))]
      (is (= 200 (:status response)))))

  (testing "greek lookup"
    (let [response ((app) (request :get "/lookup?query=Ιησούς"))]
      (is (= 200 (:status response)))))

  (testing "hebrew lookup"
    (let [response ((app) (request :get "/lookup?query=בְּרֵאשִׁית"))]
      (is (= 200 (:status response)))))
  )
